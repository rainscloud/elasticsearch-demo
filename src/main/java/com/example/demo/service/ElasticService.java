package com.example.demo.service;

/**
 * elasticsearch_demo
 * 2019/12/5 16:18
 *
 * @author
 * @description
 **/

import com.example.demo.domain.DocBean;
import org.springframework.data.domain.Page;

import java.util.Iterator;
import java.util.List;

public interface ElasticService {

    void createIndex();

    void deleteIndex(String index);

    void save(DocBean docBean);

    void saveAll(List<DocBean> list);

    Iterator<DocBean> findAll();

    Page<DocBean> findByContent(String content);

    Page<DocBean> findByFirstCode(String firstCode);

    Page<DocBean> findBySecondCode(String SecondCode);

}


